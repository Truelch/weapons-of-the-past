local weaponTexts = {
--KV-2 Mech aka 'AT-ST'
Prime_M10THowitzerArtillery_Name = "M-10T Howitzer",
Prime_M10THowitzerArtillery_Description = "A powerful cannon that deals an amount of damage equals to half of target's remaining HP (rounded up) and ignore its armor.\nMax range: 3.",
Prime_M10THowitzerArtillery_Upgrade1 = "+1 Damage",
Prime_M10THowitzerArtillery_A_UpgradeDescription = "Deals 1 additional damage.",

--PE-8
Brute_FAB500_Name = "FAB-500",
Brute_FAB500_Description = "Fly over a target, dropping a powerful bomb that damages and pulls it.",
Brute_FAB500_Upgrade1 = "+1 Range",
Brute_FAB500_Upgrade2 = "+1 Range",
Brute_FAB500_A_UpgradeDescription = "Allows dropping payload on 1 more tile per strike. (Powering both upgrades add incendiary effect to the attack)",
Brute_FAB500_B_UpgradeDescription = "Allows dropping payload on 1 more tile per strike. (Powering both upgrades add incendiary effect to the attack)",

Brute_FAB5000_Name = "FAB-5000",
Brute_FAB5000_Description = "Drops a bomb that deals 3 damage to everything in a cross-shaped area.\nSingle use.\n\nDue to the complexity of the production of this weapon, you have to complete a mission without it after use.",
Brute_FAB5000_Upgrade1 = "+2 Damage",
Brute_FAB5000_A_UpgradeDescription = "Deals 2 additional damage.",
}

return weaponTexts